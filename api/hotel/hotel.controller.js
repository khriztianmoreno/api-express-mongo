/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/hotels              ->  index
 * GET     /api/hotels/:id          ->  show
 */

const Hotel = require('./hotel.model');

function respondWithResult(res, statusCode) {
  const statusCodeLocal = statusCode || 200;
  return (entity) => {
    if (entity) {
      res.status(statusCodeLocal).json(entity);
    }
    return null;
  };
}

function saveUpdates(updates) {
  return (entity) => {
    const updated = _.merge(entity, updates);
    return updated.save().then(updatedLocal => updatedLocal);
  };
}

function handleEntityNotFound(res) {
  return (entity) => {
    if (!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  const statusCodeLocal = statusCode || 500;
  return (err) => {
    res.status(statusCodeLocal).send(err);
  };
}

// Gets a list of Hotels
function index(req, res) {
  return Hotel.find().sort({ createdAt: -1 }).exec()
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Creates a new Hotel in the DB
function create(req, res) {
  return Hotel.create(req.body)
    .then(respondWithResult(res, 201))
    .catch(handleError(res));
}


// Gets a single Hotel from the DB
function show(req, res) {
  return Hotel.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Updates an existing Hotel in the DB
function update(req, res) {
  return Hotel.findByIdAndUpdate({
      _id: req.body._id,
    }, req.body, {
      upsert: true,
    }).exec()
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Deletes a Hotel from the DB
function destroy(req, res) {
  if (req.body._id) {
    delete req.body._id;
  }

  return Hotel.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(saveUpdates(req.body))
    .then(respondWithResult(res))
    .catch(handleError(res));
}


function find(req, res) {
  const { filter } = req.params;

  const query = {
    name: filter,
  };

  if (filter) {
    return Hotel.find(query).exec()
    .then(handleEntityNotFound(res))
    .then(respondWithResult(res))
    .catch(handleError(res));
  }

  return Hotel.find().sort({ createdAt: -1 }).exec()
    .then(respondWithResult(res))
    .catch(handleError(res));
}


module.exports = {
  index,
  create,
  show,
  find,
  update,
  destroy,
};
