/**
 * Hotel
 */

const Router = require('express').Router;
const controller = require('./hotel.controller');

const router = new Router();

router.get('/', controller.index);
router.get('/search/:filter', controller.find);
router.post('/', controller.create);
router.put('/:id', controller.update);
router.delete('/:id', controller.destroy);
router.get('/:id', controller.show);

module.exports = router;
