/**
 * Main application routes
 */

// Import Endpoints
const hotel = require('./api/hotel');

module.exports = (app) => {
  // Insert routes below
  app.use('/api/hotels', hotel);
};
