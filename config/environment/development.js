/**
 * Development specific configuration
 */

module.exports = {

  // MongoDB connection options
  mongo: {
    uri: 'mongodb://dbuser:g33kIDS*@ds127173.mlab.com:27173/api-hotel-kmz',
    //uri: 'mongodb://localhost/api-hotel-dev',
  },

  // Seed database on startup
  seedDB: false,

};
